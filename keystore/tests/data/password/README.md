A keystore with some password-protected keys.

  - Alice's key is unlocked.

  - Bob's key is provided in two version: unlocked and locked with
    the password "bar".

  - Frank's key is lock with the password "foo".

```
sq key generate --userid '<alice@example.org>' -o alice.pgp --expiry never
sq key generate --userid '<bob@example.org>' -o bob.pgp --expiry never --with-password
sq key password < bob.pgp > bob-unlocked.pgp
sq key generate --userid '<frank@example.org>' -o frank.pgp --expiry never --with-password
```

Files are named after the recipients (in the order of the PKESKs),
e.g., `for-alice.pgp`.  The content is the filename without the
extension plus a newline:

```text
$ echo 123 | sq encrypt --recipient-file alice.pgp --recipient-file bob.pgp > for-alice-bob.pgp
$ echo 123 | sq encrypt --recipient-file bob.pgp --recipient-file alice.pgp > for-bob-alice.pgp
$ echo 123 | sq encrypt --recipient-file bob.pgp > for-bob.pgp
$ echo 123 | sq encrypt --recipient-file frank.pgp > for-frank.pgp
$ echo 123 | sq encrypt --recipient-file frank.pgp --recipient-file bob.pgp > for-frank-bob.pgp
```

```text
sq inspect alice.pgp
alice.pgp: Transferable Secret Key.

    Fingerprint: B9D905221577435EC69E6C10F9DF67187D2BF8B2
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-01-15 08:55:28 UTC
      Key flags: certification

         Subkey: B3F8F25FC629E058009F282928EBE800BE51B38D
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-01-15 08:55:28 UTC
      Key flags: authentication

         Subkey: 90D35FC8143312947489EDB6EB0F6B45C6FFB0D4
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-01-15 08:55:28 UTC
      Key flags: signing

         Subkey: 9B980721629F4A98332438635AE7278072A2D0D2
Public-key algo: ECDH
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-01-15 08:55:28 UTC
      Key flags: transport encryption, data-at-rest encryption

         UserID: <alice@example.org>

$ sq inspect bob.pgp
bob.pgp: Transferable Secret Key.

    Fingerprint: 7E0622ECF641D2FFE9A47C854A04BA480CE3E102
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Encrypted
  Creation time: 2024-01-15 08:59:03 UTC
      Key flags: certification

         Subkey: 58F4A44FC0F5AF61113F56F538E06AC6F5941919
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Encrypted
  Creation time: 2024-01-15 08:59:03 UTC
      Key flags: signing

         Subkey: 73628A0446F6F7F235893DDE674AE5654256C0A6
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Encrypted
  Creation time: 2024-01-15 08:59:03 UTC
      Key flags: authentication

         Subkey: 568078BB9A65285B8A7B4498E3B63B9AE4D10DD5
Public-key algo: ECDH
Public-key size: 256 bits
     Secret key: Encrypted
  Creation time: 2024-01-15 08:59:03 UTC
      Key flags: transport encryption, data-at-rest encryption

         UserID: <bob@example.org>

$ sq inspect bob-unlocked.pgp
bob-unlocked.pgp: Transferable Secret Key.

    Fingerprint: 7E0622ECF641D2FFE9A47C854A04BA480CE3E102
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-01-15 08:59:03 UTC
      Key flags: certification

         Subkey: 58F4A44FC0F5AF61113F56F538E06AC6F5941919
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-01-15 08:59:03 UTC
      Key flags: signing

         Subkey: 73628A0446F6F7F235893DDE674AE5654256C0A6
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-01-15 08:59:03 UTC
      Key flags: authentication

         Subkey: 568078BB9A65285B8A7B4498E3B63B9AE4D10DD5
Public-key algo: ECDH
Public-key size: 256 bits
     Secret key: Unencrypted
  Creation time: 2024-01-15 08:59:03 UTC
      Key flags: transport encryption, data-at-rest encryption

         UserID: <bob@example.org>

$ sq inspect frank.pgp
frank.pgp: Transferable Secret Key.

    Fingerprint: D7347AEF97CCAF209FCC4DBBFEB42E3C9D46ED92
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Encrypted
  Creation time: 2024-01-15 09:00:22 UTC
      Key flags: certification

         Subkey: 5D05E0A24D11DD97C2C08588BC5F84DB378ED544
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Encrypted
  Creation time: 2024-01-15 09:00:22 UTC
      Key flags: authentication

         Subkey: 6428BE0964CF04D8A3972B45C695C3375D14D868
Public-key algo: EdDSA
Public-key size: 256 bits
     Secret key: Encrypted
  Creation time: 2024-01-15 09:00:22 UTC
      Key flags: signing

         Subkey: 9FB98AB6ADEBCCD0D11943258738B7C8950072AE
Public-key algo: ECDH
Public-key size: 256 bits
     Secret key: Encrypted
  Creation time: 2024-01-15 09:00:22 UTC
      Key flags: transport encryption, data-at-rest encryption

         UserID: <frank@example.org>
```
