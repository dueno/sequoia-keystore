use std::collections::BTreeMap;
use std::collections::btree_map::Entry as BTreeEntry;
use std::sync::Arc;

use tokio::sync::Mutex;

use sequoia_openpgp as openpgp;
use openpgp::Result;

use std::path::Path;
use std::path::PathBuf;

use sequoia_keystore_softkeys as softkeys;
use sequoia_keystore_gpg_agent as gpg_agent;
use sequoia_keystore_backend::Backend;

/// The keystore servers.
///
/// We have one root for each "home" directory.
pub struct Servers {
    servers: BTreeMap<PathBuf, Arc<Mutex<Server>>>,
}

impl Servers {
    const fn new() -> Self {
        Servers {
            servers: BTreeMap::new(),
        }
    }

    /// Returns the server for the specified "home" directory.
    ///
    /// To use the default, use `Server::home(None)`.
    pub(crate) async fn get(home: &Path) -> Result<Arc<Mutex<Server>>> {
        let mut servers = SERVERS.lock().await;
        let home = home.to_path_buf();

        match servers.servers.entry(home.clone()) {
            BTreeEntry::Occupied(oe) => Ok(Arc::clone(oe.get())),
            BTreeEntry::Vacant(ve) => {
                let server = Arc::new(Mutex::new(Server::new(home).await?));

                ve.insert(Arc::clone(&server));

                Ok(server)
            }
        }
    }
}

lazy_static::lazy_static! {
    /// A thread-safe reference to the SERVERS singleton.
    static ref SERVERS: Mutex<Servers> = {
        Mutex::new(Servers::new())
    };
}

/// A Server.
pub struct Server {
    pub backends: Vec<Box<dyn Backend + Send + Sync>>,
}

impl Server {
    /// Returns the specified path, or the default, if `None` is
    /// provided.
    pub(crate) fn home(home: Option<&Path>) -> Result<PathBuf> {
        let home = if let Some(home) = home {
            PathBuf::from(home)
        } else if let Ok(home) = std::env::var("SQHOME") {
            PathBuf::from(home)
        } else if let Some(home) = dirs::home_dir() {
            PathBuf::from(home).join(".sq")
        } else {
            return Err(anyhow::anyhow!("Failed get the home directory"));
        };

        let home = home.join("keystore");

        Ok(home)
    }

    /// Initializes the server.
    ///
    /// This may be called at most once.
    ///
    /// `home` is the home directory to use.  If `None`, and the
    /// environment variable `SQHOME` is set, this uses `$SQHOME`.
    /// Otherwise, uses `$HOME/.sq`.  Each backend is initialized with
    /// `.../.sq/keystore/$BACKEND`.
    async fn new(home: PathBuf) -> Result<Self> {
        log::info!("Server::new({})", home.display());

        let mut backends = Vec::new();

        let mut add = |backend: Result<Box<dyn Backend + Send + Sync>>| {
            match backend {
                Ok(backend) => backends.push(backend),
                Err(err) => {
                    log::error!("Failed to initialize backend: {:#}", err);
                }
            }
        };

        add(softkeys::Backend::init(Some(home.clone().join("softkeys"))).await
            .map(|b| {
                b as Box<dyn sequoia_keystore_backend::Backend + Send + Sync>
            }));
        add(gpg_agent::Backend::init("gpg-agent").await
            .map(|b| {
                Box::new(b) as Box<dyn sequoia_keystore_backend::Backend + Send + Sync>
            }));

        log::info!("Server::new({}) -> {} backends",
                   home.display(), backends.len());

        Ok(Server {
            backends,
        })
    }
}

impl Server {
    pub fn iter(&self)
        -> impl Iterator<Item=&Box<dyn Backend + Send + Sync>> + ExactSizeIterator
    {
        self.backends.iter()
    }
}

