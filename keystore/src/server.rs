use std::path::PathBuf;
use std::sync::Arc;

use capnp::capability::Promise;

use sequoia_ipc::capnp_rpc as capnp_rpc;
use capnp_rpc::pry;
use capnp_rpc::rpc_twoparty_capnp::Side;
use capnp_rpc::{RpcSystem, twoparty};

use sequoia_openpgp as openpgp;
use openpgp::crypto::mpi::Ciphertext;
use openpgp::packet::Key;
use openpgp::packet::PKESK;
use openpgp::packet::key;
use openpgp::parse::Parse;
use openpgp::types::HashAlgorithm;
use openpgp::types::PublicKeyAlgorithm;

use sequoia_ipc as ipc;

use crate::keystore_protocol_capnp::keystore;

use crate::Error;
use crate::Result;
use crate::error::ServerError;

mod backend;
use backend::Servers;

type JoinResult<T> = std::result::Result<T, tokio::task::JoinError>;

pub struct Keystore {
    #[allow(unused)]
    descriptor: ipc::Descriptor,
    ks: keystore::Client,
}

impl Keystore {
    /// Instantiates a new `Keystore` server.
    pub fn new(descriptor: ipc::Descriptor,
               _local: &tokio::task::LocalSet)
        -> Result<Self>
    {
        // The application may not use the rust logging framework.
        // Try to initialize it so that the user can get some output.
        let _ = env_logger::Builder::from_default_env().try_init();

        let home = descriptor.context().home();
        let home = backend::Server::home(Some(home))?.to_path_buf();
        log::debug!("KeystoreServer::new: keystore directory: {}",
                    home.display());
        Ok(Keystore {
            descriptor,
            ks: capnp_rpc::new_client(KeystoreServer::new(Arc::new(home))),
        })
    }

    /// A generic variant of [`Keystore::new`].
    ///
    /// This is a variant of [`Keystore::new`], which returns a
    /// generic type instead of a [`Keystore`].  This is useful in
    /// conjunction with [`sequoia_ipc::Descriptor::new`].
    pub fn new_descriptor(descriptor: ipc::Descriptor,
                          local: &tokio::task::LocalSet)
        -> Result<Box<dyn ipc::Handler>>
    {
        Self::new(descriptor, local).map(|d| Box::new(d) as Box<dyn ipc::Handler>)
    }
}

impl ipc::Handler for Keystore {
    fn handle(&self,
              network: twoparty::VatNetwork<tokio_util::compat::Compat<tokio::net::tcp::OwnedReadHalf>>)
        -> RpcSystem<Side>
    {
        RpcSystem::new(Box::new(network), Some(self.ks.clone().client))
    }
}

#[derive(Clone, Debug)]
struct KeystoreServer {
    home: Arc<PathBuf>,
}

impl KeystoreServer {
    fn new(home: Arc<PathBuf>) -> Self
    {
        Self {
            home,
        }
    }
}

// Like try!, but stores the error in `$results`.
macro_rules! ary {
    ($results:expr, $r:expr) => {
        match $r {
            Ok(r) => r,
            Err(err) => {
                pry!($results.get().get_result())
                    .init_err()
                    .from_anyhow(&anyhow::Error::from(err));
                return Promise::ok(());
            }
        }
    }
}

// Server stubs run in three phases:
//
// - Phase 1 extracts the parameters, and copies any required data from
//   self.
//
// - Phase 2 executes the method's body in a separate async task so
//   that tasks that are bound to this thread are not blocked.
//
// - Phase 3 serializes the result in the main thread.
//
// This macro avoids some boilerplate in phase 2 and phase 3.
//
// `phase2` is an async block, which runs the server's body.  It is
// spawned as a separate async task, which, unlike the current task,
// can run on other threads.
//
// `phase3` is a closure, which is fed phase 2's return value, and
// serializes the results.
macro_rules! run {
    ($results: expr, $phase2: expr, $phase3: expr) => {
        Promise::from_future(async move {
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            let r: JoinResult<Result<_>> = tokio::task::spawn($phase2).await;

            // Convert a join error to an internal server error.
            let r = match r {
                Ok(r) => r,
                Err(err) => {
                    let err = err.to_string();
                    let mut builder = $results.get().get_result()?.init_err()
                        .init_internal_error(err.len() as u32);
                    builder.push_str(&err);

                    return Ok(());
                }
            };

            // Phase 3: Serialize the result in the main thread.
            let r: Result<_> = r.and_then($phase3);

            if let Err(err) = r {
                $results.get().get_result()?
                    .init_err()
                    .from_anyhow(&anyhow::Error::from(err));
            }
            Ok(())
        })
    };
}

impl keystore::Server for KeystoreServer {
    fn backends(&mut self,
                _params: keystore::BackendsParams,
                mut results: keystore::BackendsResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let home = Arc::clone(&self.home);
        let home2 = Arc::clone(&self.home);

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let server = server.lock().await;

                let ids = server.iter()
                    .map(|backend| backend.id())
                    .collect::<Vec<String>>();

                Ok(ids)
            },
            // Phase 3: Serialize the result in the main thread.
            |ids: Vec<String>| {
                let mut ok = results.get().get_result()?
                    .initn_ok(ids.len() as u32);
                for (i, id) in ids.into_iter().enumerate() {
                    let server: BackendServer = BackendServer::new(
                        Arc::clone(&home2), id);
                    let cap: keystore::backend::Client
                        = capnp_rpc::new_client(server);
                    ok.set(i as u32, cap.client.hook);
                }

                Ok(())
            })
    }

    fn decrypt(&mut self,
                params: keystore::DecryptParams,
                mut results: keystore::DecryptResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let mut pkesks = Vec::new();
        for (i, pkesk) in pry!(pry!(params.get()).get_pkesks()).iter().enumerate() {
            pkesks.push((i, ary!(results, PKESK::from_bytes(pry!(pkesk)))));
        }

        let home = Arc::clone(&self.home);

        log::trace!("KeystoreServer::decrypt({})",
                    pkesks.iter()
                    .map(|(_i, pkesk)| {
                        pkesk.recipient().to_string()
                    })
                    .collect::<Vec<String>>()
                    .join(", "));

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let (wildcard, known): (Vec<&(usize, PKESK)>, Vec<&(usize, PKESK)>)
                    = pkesks
                    .iter()
                    .partition(|(_i, pkesk)| pkesk.recipient().is_wildcard());

                // Keys that are inaccessible, but that could become available
                // with a little help from the user (e.g., unlock a password,
                // insert a device, etc.)
                let mut inaccessible: Vec<InaccessibleDecryptionKey> = Vec::new();

                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if ! known.is_empty() {
                    for backend in server.backends.iter_mut() {
                        for device in backend.list().await {
                            for mut key in device.list().await {
                                for (i, pkesk) in known.iter() {
                                    let keyid = key.keyid();
                                    if pkesk.recipient() != &keyid {
                                        log::trace!("PKESK for {}, key: {}, skipping",
                                                    pkesk.recipient(), keyid);
                                        continue;
                                    }

                                    if ! key.decryption_capable().await {
                                        log::trace!("{} cannot be used for decryption",
                                                    key.fingerprint());
                                        continue;
                                    }

                                    let mut abort = false;
                                    if key.locked().await {
                                        log::trace!("{} is locked", keyid);
                                        abort = true;
                                    }
                                    if ! key.available().await {
                                        log::trace!("{} is unavailable",
                                                    key.fingerprint());
                                        abort = true;
                                    }
                                    if abort {
                                        inaccessible.push(InaccessibleDecryptionKey {
                                            key: KeyDescriptor {
                                                server: KeyServer::new(
                                                    Arc::clone(&home),
                                                    backend.id(),
                                                    device.id(), key.id()),
                                                key: key.public_key().await.clone(),
                                            },
                                            pkesk: pkesk.clone(),
                                        });
                                        continue;
                                    }

                                    if let Some((sym_algo, sk))
                                        = key.decrypt_pkesk(pkesk).await
                                    {
                                        let fpr = key.fingerprint().as_bytes().to_vec();
                                        return Ok((*i, fpr, sym_algo, sk));
                                    } else {
                                        log::trace!("PKESK for {} matches key {}, \
                                                     but failed to decrypt it!",
                                                    pkesk.recipient(), keyid);
                                    }
                                }
                            }
                        }
                    }
                }

                if ! wildcard.is_empty() {
                    unimplemented!();
                }

                if ! inaccessible.is_empty() {
                    log::debug!("Failed to decrypt, but have {} candidate keys \
                                 that are inaccessible",
                                inaccessible.len());
                    Err(ServerError::InaccessibleDecryptionKey(inaccessible).into())
                } else {
                    // XXX: The right error is: no known secret key can decrypt
                    // it.
                    Err(Error::EOF.into())
                }
            },
            // Phase 3: Serialize the result.
            |(i, fingerprint, sym_algo, sk)| {
                let mut ok = results.get().get_result()?.init_ok();

                ok.set_index(i as u32);
                ok.set_fingerprint(&fingerprint);
                ok.set_algo(u8::from(sym_algo));
                ok.set_session_key(&sk);

                Ok::<_, anyhow::Error>(())
            })
    }
}

#[derive(Clone, Debug)]
struct BackendServer {
    home: Arc<PathBuf>,
    id: String,
}

impl BackendServer {
    fn new<S: AsRef<str>>(home: Arc<PathBuf>, id: S) -> Self {
        let server = Self {
            home,
            id: id.as_ref().into(),
        };

        server
    }
}

impl keystore::backend::Server for BackendServer {
    fn id(&mut self,
          _params: keystore::backend::IdParams,
          mut results: keystore::backend::IdResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        let mut ok = pry!(results.get().get_result());
        pry!(ok.set_ok(&self.id));

        Promise::ok(())
    }

    fn list(&mut self, _params: keystore::backend::ListParams,
            mut results: keystore::backend::ListResults)
            -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let home = Arc::clone(&self.home);
        let home2 = Arc::clone(&self.home);
        let backend_id = self.id.clone();
        let backend_id2 = self.id.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    let devices: Vec<_> = backend.list().await.collect();
                    let ids = devices.into_iter()
                        .map(|d| d.id())
                        .collect::<Vec<String>>();

                    Ok(ids)
                } else {
                    Err(Error::EOF.into())
                }
            },
            // Phase 3: Serialize the result.
            |ids: Vec<String>| {
                let mut ok = results.get().get_result()?
                    .initn_ok(ids.len() as u32);
                for (i, device_id) in ids.into_iter().enumerate() {
                    let server = DeviceServer::new(
                        Arc::clone(&home2), &backend_id2, device_id);
                    let cap: keystore::device::Client
                        = capnp_rpc::new_client(server);
                    ok.set(i as u32, cap.client.hook);
                }

                Ok::<(), anyhow::Error>(())
            })
    }
}

#[derive(Clone, Debug)]
struct DeviceServer {
    home: Arc<PathBuf>,
    backend: String,
    id: String,
}

impl DeviceServer {
    fn new<B, I>(home: Arc<PathBuf>, backend: B, id: I) -> Self
        where B: AsRef<str>, I: AsRef<str>
    {
        let device = Self {
            home,
            backend: backend.as_ref().into(),
            id: id.as_ref().into(),
        };

        device
    }
}

impl keystore::device::Server for DeviceServer {
    fn id(&mut self,
          _params: keystore::device::IdParams,
          mut results: keystore::device::IdResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        let mut ok = pry!(results.get().get_result());
        pry!(ok.set_ok(&self.id));

        Promise::ok(())
    }

    fn list(&mut self, _params: keystore::device::ListParams,
            mut results: keystore::device::ListResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let home = Arc::clone(&self.home);
        let home2 = Arc::clone(&self.home);
        let backend_id = self.backend.clone();
        let backend_id2 = self.backend.clone();
        let device_id = self.id.clone();
        let device_id2 = self.id.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    if let Some(device) = backend.list().await
                        .find(|d| d.id() == device_id)
                    {
                        let mut info = Vec::new();
                        let keys: Vec<_> = device.list().await.collect();
                        for key in keys.into_iter() {
                            let key_id = key.id();

                            use openpgp::serialize::MarshalInto;
                            let key_bytes = key.public_key().await.to_vec()
                                .expect("serializing to a vec is infallible");

                            info.push((key_id, key_bytes));
                        }
                        Ok(info)
                    } else {
                        Err(Error::EOF.into())
                    }
                } else {
                    Err(Error::EOF.into())
                }
            },
            // Phase 3: Serialize the result.
            |info: Vec<(String, Vec<u8>)>| {
                let mut ok = results.get().get_result()?
                    .initn_ok(info.len() as u32);
                for (i, (key_id, key_bytes)) in info.into_iter().enumerate() {
                    let server = KeyServer::new(
                        Arc::clone(&home2), &backend_id2, &device_id2, key_id);
                    let cap: keystore::key::Client
                        = capnp_rpc::new_client(server);

                    let mut r = ok.reborrow().get(i as u32);
                    r.set_handle(cap);
                    r.set_public_key(&key_bytes[..]);
                }

                Ok::<(), anyhow::Error>(())
            })
    }
}

#[derive(Clone, Debug)]
struct KeyServer {
    home: Arc<PathBuf>,
    backend: String,
    device: String,
    key: String,
}

impl KeyServer {
    fn new<B, D, K>(home: Arc<PathBuf>, backend: B, device: D, key: K)
        -> Self
        where B: AsRef<str>, D: AsRef<str>, K: AsRef<str>
    {
        let key = Self {
            home: home,
            backend: backend.as_ref().into(),
            device: device.as_ref().into(),
            key: key.as_ref().into(),
        };

        key
    }
}

impl keystore::key::Server for KeyServer {
    fn id(&mut self,
          _params: keystore::key::IdParams,
          mut results: keystore::key::IdResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        let mut ok = pry!(results.get().get_result());
        pry!(ok.set_ok(&self.key));

        Promise::ok(())
    }

    fn unlock(&mut self,
              params: keystore::key::UnlockParams,
              mut results: keystore::key::UnlockResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let p = pry!(params.get());
        let password: Vec<u8> = pry!(p.get_password()).to_vec();

        let home = Arc::clone(&self.home);
        let backend_id = self.backend.clone();
        let device_id = self.device.clone();
        let key_id = self.key.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    if let Some(device) = backend.list().await
                        .find(|d| d.id() == device_id)
                    {
                        if let Some(mut key) = device.list().await
                            .find(|k| k.id() == key_id)
                        {
                            match key.unlock(&password.into()).await {
                                Ok(()) => return Ok(()),
                                Err(err) => return Err(err.into()),
                            }
                        }
                    }
                }

                Err(Error::EOF.into())
            },
            // Phase 3: Serialize the result in the main thread.
            |()| {
                let _ok = results.get().get_result()?.set_ok(());
                Ok(())
            })
    }

    fn decrypt_ciphertext(&mut self,
                          params: keystore::key::DecryptCiphertextParams,
                          mut results: keystore::key::DecryptCiphertextResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let algo = PublicKeyAlgorithm::from(pry!(params.get()).get_algo());
        let ciphertext = pry!(pry!(params.get()).get_ciphertext());
        let ciphertext = ary!(results, Ciphertext::parse(algo, ciphertext));

        let plaintext_len = match pry!(params.get()).get_plaintext_len() {
            0 => None,
            n => Some(n as usize),
        };

        let home = Arc::clone(&self.home);
        let backend_id = self.backend.clone();
        let device_id = self.device.clone();
        let key_id = self.key.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    if let Some(device) = backend.list().await
                        .find(|d| d.id() == device_id)
                    {
                        if let Some(mut key) = device.list().await
                            .find(|k| k.id() == key_id)
                        {
                            if ! key.decryption_capable().await {
                                log::trace!("{} is not for decryption",
                                            key.fingerprint());
                                return Err(Error::NotDecryptionCapable(
                                    key.fingerprint().to_string()).into());
                            }

                            match key.decrypt_ciphertext(
                                &ciphertext, plaintext_len).await
                            {
                                Ok(sk) => return Ok(sk),
                                Err(err) => return Err(err.into()),
                            }
                        }
                    }
                }

                Err(Error::EOF.into())
            },
            // Phase 3: Serialize the result in the main thread.
            |sk| {
                let ok = results.get().get_result()?
                    .initn_ok(sk.len() as u32);
                ok.copy_from_slice(&sk);

                Ok(())
            })
    }

    fn sign_message(&mut self,
                    params: keystore::key::SignMessageParams,
                    mut results: keystore::key::SignMessageResults)
            -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let p = pry!(params.get());
        let hash_algo = HashAlgorithm::from(p.get_hash_algo());
        let digest: Vec<u8> = pry!(p.get_digest()).to_vec();

        let home = Arc::clone(&self.home);
        let backend_id = self.backend.clone();
        let device_id = self.device.clone();
        let key_id = self.key.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    if let Some(device) = backend.list().await
                        .find(|d| d.id() == device_id)
                    {
                        if let Some(mut key) = device.list().await
                            .find(|k| k.id() == key_id)
                        {
                            log::trace!("Signing digest with {} (hash: {})",
                                        key.keyid(), hash_algo);

                            if ! key.signing_capable().await {
                                log::trace!("{} is not for signing",
                                            key.fingerprint());
                                return Err(Error::NotSigningCapable(
                                    key.fingerprint().to_string()).into());
                            }
                            match key.sign(hash_algo, &digest).await {
                                Ok((pk_algo, sig)) => {

                                    use openpgp::serialize::MarshalInto;
                                    let bytes = sig.to_vec()
                                        .expect("serializing to a vec is infallible");
                                    return Ok((pk_algo, bytes));
                                }
                                Err(err) => {
                                    log::info!("Failed to sign {} digest with {}: {}",
                                               hash_algo, key.keyid(), err);
                                    return Err(err.into());
                                }
                            }
                        }
                    }
                }

                log::debug!("Invalid key capability: {}/{}/{} not found",
                            backend_id, device_id, key_id);

                return Err(Error::EOF.into());
            },
            // Phase 3: Serialize the result in the main thread.
            |(pk_algo, bytes)| {
                let mut ok = results.get().get_result()?.init_ok();
                ok.set_pk_algo(u8::from(pk_algo));

                let mpis = ok.init_mpis(bytes.len() as u32);
                mpis.copy_from_slice(&bytes);

                Ok(())
            })
    }

    fn available(&mut self,
                 _params: keystore::key::AvailableParams,
                 mut results: keystore::key::AvailableResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let home = Arc::clone(&self.home);
        let backend_id = self.backend.clone();
        let device_id = self.device.clone();
        let key_id = self.key.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    if let Some(device) = backend.list().await
                        .find(|d| d.id() == device_id)
                    {
                        if let Some(key) = device.list().await
                            .find(|k| k.id() == key_id)
                        {
                            return Ok(key.available().await);
                        }
                    }
                }

                Err(Error::EOF.into())
            },
            // Phase 3: Serialize the result in the main thread.
            |available: bool| {
                let _ok = results.get().get_result()?.set_ok(available);
                Ok(())
            })
    }

    fn locked(&mut self,
              _params: keystore::key::LockedParams,
              mut results: keystore::key::LockedResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let home = Arc::clone(&self.home);
        let backend_id = self.backend.clone();
        let device_id = self.device.clone();
        let key_id = self.key.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    if let Some(device) = backend.list().await
                        .find(|d| d.id() == device_id)
                    {
                        if let Some(key) = device.list().await
                            .find(|k| k.id() == key_id)
                        {
                            return Ok(key.locked().await);
                        }
                    }
                }

                Err(Error::EOF.into())
            },
            // Phase 3: Serialize the result in the main thread.
            |locked: bool| {
                let _ok = results.get().get_result()?.set_ok(locked);
                Ok(())
            })
    }

    fn decryption_capable(&mut self,
                          _params: keystore::key::DecryptionCapableParams,
                          mut results: keystore::key::DecryptionCapableResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let home = Arc::clone(&self.home);
        let backend_id = self.backend.clone();
        let device_id = self.device.clone();
        let key_id = self.key.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    if let Some(device) = backend.list().await
                        .find(|d| d.id() == device_id)
                    {
                        if let Some(key) = device.list().await
                            .find(|k| k.id() == key_id)
                        {
                            return Ok(key.decryption_capable().await);
                        }
                    }
                }

                Err(Error::EOF.into())
            },
            // Phase 3: Serialize the result in the main thread.
            |decryption_capable: bool| {
                let _ok = results.get().get_result()?.set_ok(decryption_capable);
                Ok(())
            })
    }

    fn signing_capable(&mut self,
                       _params: keystore::key::SigningCapableParams,
                       mut results: keystore::key::SigningCapableResults)
        -> ::capnp::capability::Promise<(), ::capnp::Error>
    {
        // Phase 1: Extract the parameters, and copy any required data
        // from self.
        let home = Arc::clone(&self.home);
        let backend_id = self.backend.clone();
        let device_id = self.device.clone();
        let key_id = self.key.clone();

        run!(
            results,
            // Phase 2: Execute the method's body in a separate task
            // so that tasks that are bound to this thread are not
            // blocked.
            async move {
                let server = Servers::get(home.as_path()).await?;
                let mut server = server.lock().await;

                if let Some(backend) = server.backends.iter_mut()
                    .find(|b| b.id() == backend_id)
                {
                    if let Some(device) = backend.list().await
                        .find(|d| d.id() == device_id)
                    {
                        if let Some(key) = device.list().await
                            .find(|k| k.id() == key_id)
                        {
                            return Ok(key.signing_capable().await);
                        }
                    }
                }

                Err(Error::EOF.into())
            },
            // Phase 3: Serialize the result in the main thread.
            |signing_capable: bool| {
                let _ok = results.get().get_result()?.set_ok(signing_capable);
                Ok(())
            })
    }
}

// Basically, a wrapper around a KeyHandle that we want to return
// later.  This isn't just a wrapper around a `dyn
// sequoia_keystore_backend::KeyHandle` due to lifetimes.
#[derive(Clone)]
pub(crate) struct KeyDescriptor {
    server: KeyServer,
    key: Key<key::PublicParts, key::UnspecifiedRole>,
}

impl std::fmt::Debug for KeyDescriptor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "server::KeyDescriptor {{ {} }}",
               self.key.fingerprint())
    }
}

impl KeyDescriptor {
    /// Returns a capability.
    pub fn cap(&self) -> keystore::key::Client {
        capnp_rpc::new_client(self.server.clone())
    }

    /// Returns the key handle.
    pub fn public_key(&self) -> &Key<key::PublicParts, key::UnspecifiedRole> {
        &self.key
    }

    pub fn serialize(&self, mut builder: keystore::key_descriptor::Builder) {
        use openpgp::serialize::MarshalInto;

        builder.set_handle(self.cap());

        let bytes = self.public_key().to_vec()
            .expect("serializing to a vec is infallible");
        builder.set_public_key(&bytes[..]);
    }
}

#[derive(Clone)]
pub(crate) struct InaccessibleDecryptionKey {
    key: KeyDescriptor,
    pkesk: PKESK,
}

impl std::fmt::Debug for InaccessibleDecryptionKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "server::InaccessibleDecryptionKey {{ {} }}",
               self.key.key.fingerprint())
    }
}

impl InaccessibleDecryptionKey {
    pub fn serialize(&self, mut builder: keystore::inaccessible_decryption_key::Builder) {
        use openpgp::serialize::MarshalInto;

        self.key.serialize(builder.reborrow().init_key_descriptor());

        let bytes = self.pkesk.to_vec()
            .expect("serializing to a vec is infallible");
        builder.set_pkesk(&bytes[..]);
    }
}
